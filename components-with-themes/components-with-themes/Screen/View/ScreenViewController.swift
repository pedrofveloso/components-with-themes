//
//  ModalScreenViewController.swift
//  components-with-themes
//
//  Created by Pedro Veloso on 14/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol ScreenViewControllerDelegate: AnyObject {
    func setTitleLabel(text:String)
    func setDescriptionLabel(text:String)
    func setButtonTitle(text:String)
    
    func setTitleColor(color:UIColor)
    func setDescriptionColor(color: UIColor)
    func setButtonHidden(isHidden: Bool)
    func setButtonStyle(backgroundColor: UIColor, textColor: UIColor)
    func setBackgroundColor(color: UIColor)
}

class ScreenViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.numberOfLines = 0
            descriptionLabel.lineBreakMode = .byWordWrapping
        }
    }
    @IBOutlet weak var modalButton: UIButton!
    
    //MARK: - Properties
    var presenter: ScreenPresenterDelegate? {
        didSet {
            presenter?.set(view: self)
        }
    }
    
    //MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ScreenViewController: ScreenViewControllerDelegate {
    func setTitleLabel(text: String) {
        titleLabel.text = text
    }
    
    func setDescriptionLabel(text: String) {
        descriptionLabel.text = text
    }
    
    func setButtonTitle(text: String) {
        modalButton.setTitle(text, for: .normal)
    }
    
    func setTitleColor(color: UIColor) {
        titleLabel.textColor = color
    }
    
    func setDescriptionColor(color: UIColor) {
        descriptionLabel.textColor = color
    }
    
    func setButtonStyle(backgroundColor: UIColor, textColor: UIColor) {
        modalButton.backgroundColor = backgroundColor
        modalButton.setTitleColor(textColor, for: .normal)
    }
    
    func setButtonHidden(isHidden: Bool) {
        modalButton.isHidden = isHidden
    }
    
    func setBackgroundColor(color: UIColor) {
        view.backgroundColor = color
    }
}
