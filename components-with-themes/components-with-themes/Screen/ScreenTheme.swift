//
//  ModalTheme.swift
//  components-with-themes
//
//  Created by Pedro Veloso on 14/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol ThemeAttributes {
    var primaryColor: UIColor { get }
    var secondaryColor: UIColor { get }
    var isButtonHidden: Bool { get }
}

enum ScreenTheme {
    case theme1, theme2, themeN
    
    var instance: ThemeAttributes {
        switch self {
        case .theme1:
            return Theme1()
        case .theme2:
            return Theme2()
        default:
            fatalError()
        }
    }
    
}
