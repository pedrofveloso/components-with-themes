//
//  ScreenPresenter.swift
//  components-with-themes
//
//  Created by Pedro Veloso on 14/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

protocol ScreenPresenterDelegate: AnyObject {
    func set(view: ScreenViewControllerDelegate)
    func viewDidLoad()
}

class ScreenPresenter {
    weak var view: ScreenViewControllerDelegate?
    let theme: ThemeAttributes
    
    init(theme: ThemeAttributes) {
        self.theme = theme
    }
}

extension ScreenPresenter: ScreenPresenterDelegate {
    func set(view: ScreenViewControllerDelegate) {
        self.view = view
    }
    
    func viewDidLoad() {
        view?.setTitleLabel(text: "Lorem Ipsum")
        view?.setDescriptionLabel(text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque at dignissim leo, eu lobortis turpis. Morbi dapibus maximus est, in sagittis diam facilisis at. Ut placerat finibus dapibus. Nunc at laoreet leo. Nunc imperdiet maximus bibendum.")
        view?.setButtonTitle(text: "Lorem")
        
        view?.setTitleColor(color: theme.secondaryColor)
        view?.setDescriptionColor(color: theme.secondaryColor)
        view?.setBackgroundColor(color: theme.primaryColor)
        
        view?.setButtonHidden(isHidden: theme.isButtonHidden)
        
        if !theme.isButtonHidden {
            view?.setButtonStyle(backgroundColor: theme.secondaryColor, textColor: theme.primaryColor)
        }
    }
}
