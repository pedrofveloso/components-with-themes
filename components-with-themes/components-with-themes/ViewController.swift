//
//  ViewController.swift
//  components-with-themes
//
//  Created by Pedro Veloso on 14/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let theme = (indexPath.row == 0) ? ScreenTheme.theme1 : ScreenTheme.theme2
        let presenter = ScreenPresenter(theme: theme.instance)
        let vc = ScreenViewController(nibName: "\(ScreenViewController.self)", bundle: nil)
        vc.presenter = presenter
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = "Tema \(indexPath.row+1)"
        return cell
    }
    
    
}
