//
//  Theme1.swift
//  components-with-themes
//
//  Created by Pedro Veloso on 15/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

struct Theme1: ThemeAttributes {
    var primaryColor: UIColor {
        return UIColor.red
    }
    
    var secondaryColor: UIColor {
        return UIColor.white
    }
    
    var isButtonHidden: Bool {
        return false
    }
}
