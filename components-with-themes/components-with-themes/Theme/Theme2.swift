//
//  Theme2.swift
//  components-with-themes
//
//  Created by Pedro Veloso on 15/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

struct Theme2: ThemeAttributes {
    var primaryColor: UIColor {
        return UIColor.white
    }
    
    var secondaryColor: UIColor {
        return UIColor.black
    }
    
    var isButtonHidden: Bool {
        return false
    }
}
