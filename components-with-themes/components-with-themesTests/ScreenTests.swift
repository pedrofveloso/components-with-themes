//
//  components_with_themesTests.swift
//  components-with-themesTests
//
//  Created by Pedro Veloso on 14/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import XCTest
@testable import components_with_themes

class ScreenTests: XCTestCase {
    
    var theme = MockTheme()
    var viewModel: ScreenPresenterDelegate? {
        didSet {
            viewModel?.set(view: view!)
        }
    }
    var view: MockView?
    

    override func setUp() {
        view = MockView()
        viewModel = ScreenPresenter(theme: theme)
    }

    override func tearDown() {}

    func testWhenViewDidLoadAndThemeShouldHideButton() {
        theme.hiddenToBeSetted = true
        setupMockView()
        view?.isHiddenToBeSetted = true
        
        viewModel?.viewDidLoad()
        
        XCTAssertTrue(view!.setTitleLabelWasCalled, "setTitleLabel(text:) should be called")
        XCTAssertTrue(view!.setDescriptionLabelWasCalled, "setDescriptionLabel(text:) should be called")
        XCTAssertTrue(view!.setButtonTitleWasCalled, "setButtonTitle(text:) should be called")
        XCTAssertTrue(view!.setTitleColorWasCalled, "setTitleColor(color:) should be called")
        XCTAssertTrue(view!.setDecriptionColorWasCalled, "setDescriptionColor(color:) should be called")
        XCTAssertTrue(view!.setBackgroundColoWasCalled, "setBackgroundColor(color:) should be called")
        XCTAssertTrue(view!.setButtonHiddenWasCalled, "setButtonHidden(isHidden:) should be called")
        
        XCTAssertFalse(view!.setButtonStyleWasCalled, "setButtonStyle(backgroundColor:textColor:) should be called")
    }
    
    func testWhenViewDidLoadAndThemeShouldShowButton() {
        theme.hiddenToBeSetted = false
        setupMockView()
        view?.isHiddenToBeSetted = false
        
        viewModel?.viewDidLoad()
        
        XCTAssertTrue(view!.setTitleLabelWasCalled, "setTitleLabel(text:) should be called")
        XCTAssertTrue(view!.setDescriptionLabelWasCalled, "setDescriptionLabel(text:) should be called")
        XCTAssertTrue(view!.setButtonTitleWasCalled, "setButtonTitle(text:) should be called")
        XCTAssertTrue(view!.setTitleColorWasCalled, "setTitleColor(color:) should be called")
        XCTAssertTrue(view!.setDecriptionColorWasCalled, "setDescriptionColor(color:) should be called")
        XCTAssertTrue(view!.setBackgroundColoWasCalled, "setBackgroundColor(color:) should be called")
        XCTAssertTrue(view!.setButtonHiddenWasCalled, "setButtonHidden(isHidden:) should be called")
        XCTAssertTrue(view!.setButtonStyleWasCalled, "setButtonStyle(backgroundColor:textColor:) should be called")
    }
    
    private func setupMockView() {
        view?.titleToBeSetted = "Lorem Ipsum"
        view?.descriptionToBeSetted = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque at dignissim leo, eu lobortis turpis. Morbi dapibus maximus est, in sagittis diam facilisis at. Ut placerat finibus dapibus. Nunc at laoreet leo. Nunc imperdiet maximus bibendum."
        view?.buttonTitleToBeSetted = "Lorem"
        view?.primaryColorToBeSetted = UIColor.clear
        view?.secondaryColorToBeSetted = UIColor.clear
    }
    
}

class MockTheme: ThemeAttributes {
    
    var hiddenToBeSetted: Bool? = nil
    
    var primaryColor: UIColor {
        return UIColor.clear
    }
    
    var secondaryColor: UIColor {
        return UIColor.clear
    }
    
    var isButtonHidden: Bool {
        return hiddenToBeSetted!
    }
}

class MockView: ScreenViewControllerDelegate {
    var titleToBeSetted: String? = nil
    var descriptionToBeSetted: String? = nil
    var buttonTitleToBeSetted: String? = nil
    var primaryColorToBeSetted: UIColor? = nil
    var secondaryColorToBeSetted: UIColor? = nil
    var isHiddenToBeSetted: Bool? = nil
    
    var setTitleLabelWasCalled = false
    var setDescriptionLabelWasCalled = false
    var setButtonTitleWasCalled = false
    var setTitleColorWasCalled = false
    var setDecriptionColorWasCalled = false
    var setButtonHiddenWasCalled = false
    var setButtonStyleWasCalled = false
    var setBackgroundColoWasCalled = false
    
    
    func setTitleLabel(text: String) {
        XCTAssertEqual(text, titleToBeSetted)
        setTitleLabelWasCalled = true
    }
    
    func setDescriptionLabel(text: String) {
        XCTAssertEqual(text, descriptionToBeSetted)
        setDescriptionLabelWasCalled = true
    }
    
    func setButtonTitle(text: String) {
        XCTAssertEqual(text, buttonTitleToBeSetted)
        setButtonTitleWasCalled = true
    }
    
    func setTitleColor(color: UIColor) {
        XCTAssertEqual(color, secondaryColorToBeSetted)
        setTitleColorWasCalled = true
    }
    
    func setDescriptionColor(color: UIColor) {
        XCTAssertEqual(color, secondaryColorToBeSetted)
        setDecriptionColorWasCalled = true
    }
    
    func setButtonHidden(isHidden: Bool) {
        XCTAssertEqual(isHidden, isHiddenToBeSetted)
        setButtonHiddenWasCalled = true
    }
    
    func setButtonStyle(backgroundColor: UIColor, textColor: UIColor) {
        XCTAssertEqual(backgroundColor, secondaryColorToBeSetted)
        XCTAssertEqual(textColor, primaryColorToBeSetted)
        
        setButtonStyleWasCalled = true
    }
    
    func setBackgroundColor(color: UIColor) {
        XCTAssertEqual(color, primaryColorToBeSetted)
        
        setBackgroundColoWasCalled = true
    }
    
    
}
